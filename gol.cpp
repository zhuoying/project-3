/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g)
{
	char c;
	if (
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

vector<bool> v1;
vector<vector<bool> > v2;
char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

bool live_cell(int i, int j, int length, int width)
{
  int neighbor = 0;
  if(v2[i][(j-1+width)%width] == true)
    neighbor++;
  if(v2[i][(j+1)%width] == true)
    neighbor++;
  if(v2[(i-1+length)%length][j]) == true)
    neighbor++;
  if(v2[(i+1)%length][j] == true)
    neighbor++;
  if(v2[(i-1+length)%length][(j-1+width)%width] == true)
    neighbor++;
  if(v2[(i+1)%length][(j-1+width)%width] == true)
    neighbor++;
  if(v2[(i-1+length)%length][(j+1)%width] == true)
    neighbor++;
  if(v2[(i+1)%length][(j+1)%width] == true)
    neighbor++;

  if(neighbor < 2 || neighbor > 3)
    return false;
  else
    return true;
}

bool dead_cell(int i, int j, int length, int width)
{
  int neighbor = 0;
  if(v2[i][(j-1+width)%width] == true)
    neighbor++;
  if(v2[i][(j+1)%width] == true)
    neighbor++;
  if(v2[(i-1+length)%length][j]) == true)
    neighbor++;
  if(v2[(i+1)%length][j] == true)
    neighbor++;
  if(v2[(i-1+length)%length][(j-1+width)%width] == true)
    neighbor++;
  if(v2[(i+1)%length][(j-1+width)%width] == true)
    neighbor++;
  if(v2[(i-1+length)%length][(j+1)%width] == true)
    neighbor++;
  if(v2[(i+1)%length][(j+1)%width] == true)
    neighbor++;

  if(neighbor == 3)
    return true;
  else
    return false;
}

void mainLoop() {
	char c;
	int length, width;
	while(fread(&c,1,1,f) != 0)
	{
		length = v1.size();
		if(c == "\n")
		{
			v2.push_back(v1);
			v1.clear();
		}
		else
		{
			if(c == ".")
				v1.push_back(false);
			else
				v1.push_back(true);
		}
		width = v2.size();
	}
	while(fwrite(&c,1,1,f) = 0)
	{

		for(int i=0;v2[i][j] < v2.size(); i++)
		if(c == ".")
			dead_cell(v2[i][j],length, width);
		if(c == "o")
			live cell(v2[i][j],length, width);


	/* update, write, sleep */
}
